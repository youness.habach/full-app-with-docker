package com.ynshb.utils;

import com.ynshb.entities.Product;

import java.util.Objects;

public class OrderUtils {

    public static Product updateRequiredFields(Product productInDb, Product productBody) {
        String name = productBody.getName();
        String label = productBody.getLabel();
        Double price = productBody.getPrice();
        if (Objects.nonNull(name)) {
            productInDb.setName(name);
        }
        if (Objects.nonNull(label)) {
            productInDb.setLabel(label);
        }
        if (Objects.nonNull(price)) {
            productInDb.setPrice(price);
        }
        return productInDb;
    }
}
