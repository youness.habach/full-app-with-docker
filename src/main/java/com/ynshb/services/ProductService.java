package com.ynshb.services;

import com.ynshb.entities.Product;
import com.ynshb.repository.ProductRepository;
import com.ynshb.utils.OrderUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;

    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public Product getProductById(Long id) {
        Optional<Product> productById = productRepository.findById(id);
        if (productById.isPresent()) {
            return productById.get();
        }
        throw new IllegalArgumentException("No order found with id: " + id);
    }

    public Product saveProduct(Product product) {
        return productRepository.save(product);
    }

    public List<Product> saveAllProduct(List<Product> products) {
        return productRepository.saveAll(products);
    }

    public Product updateProductPrice(Long id, Double price) {
        Optional<Product> productById = productRepository.findById(id);
        if (productById.isPresent()) {
            Product product = productById.get();
            product.setPrice(price);
            return productRepository.save(product);
        }
        throw new IllegalArgumentException("No order to update the price with id: " + id);
    }

    public Product updateWholeProduct(Product productBody, Long id) {
        Optional<Product> productById = productRepository.findById(id);
        if (productById.isPresent()) {
            Product product = OrderUtils.updateRequiredFields(productById.get(), productBody);
            return productRepository.save(product);
        }
        throw new IllegalArgumentException("No order to update with id: " + id);
    }

    public void deleteProductById(Long id) {
        Optional<Product> productById = productRepository.findById(id);
        if (productById.isPresent()) {
            productRepository.deleteById(id);
        } else {
            throw new IllegalArgumentException("No order to delete with id: " + id);
        }
    }

    public void deleteAllProducts() {
        List<Product> allProducts = getAllProducts();
        if (allProducts.size() > 0) {
            productRepository.deleteAll();
        } else {
            throw new IllegalArgumentException("No order present in database to delete");
        }
    }
}







