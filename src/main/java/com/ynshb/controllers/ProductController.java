package com.ynshb.controllers;

import com.ynshb.entities.Product;
import com.ynshb.services.ProductService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/order")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    //@GetMapping
    @RequestMapping(value = "/get-all", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "This api for getting all products.")
    public List<Product> getAllProducts() {
        return productService.getAllProducts();
    }

    //@GetMapping
    @RequestMapping(value = "/get-by-id/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Product getProductById(@PathVariable("id") Long id) {
        return productService.getProductById(id);
    }

    //@PostMapping
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Product addProduct(@RequestBody Product product) {
        return productService.saveProduct(product);
    }

    //@PostMapping
    @RequestMapping(value = "/add-all", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public List<Product> addProduct(@RequestBody List<Product> products) {
        return productService.saveAllProduct(products);
    }

    //@PostMapping
    @RequestMapping(value = "/add-sample", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public List<Product> addSampleProducts() {
        List<Product> products = List.of(
                Product.builder().name("Name_1").label("Label_1").price(1000.00).build(),
                Product.builder().name("Name_2").label("Label_2").price(2000.00).build(),
                Product.builder().name("Name_3").label("Label_3").price(3000.00).build(),
                Product.builder().name("Name_4").label("Label_4").price(4000.00).build(),
                Product.builder().name("Name_5").label("Label_5").price(5000.00).build(),
                Product.builder().name("Name_6").label("Label_6").price(6000.00).build(),
                Product.builder().name("Name_7").label("Label_7").price(7000.00).build(),
                Product.builder().name("Name_8").label("Label_8").price(8000.00).build(),
                Product.builder().name("Name_9").label("Label_9").price(9000.00).build(),
                Product.builder().name("Name_10").label("Label_10").price(10000.00).build()
        );
        return productService.saveAllProduct(products);
    }

    //@GetMapping
    @RequestMapping(value = "/update-price/{id}/{price}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Product updateProductPrice(@PathVariable(value = "id") Long id, @PathVariable(value = "price") Double price) {
        return productService.updateProductPrice(id, price);
    }

    //@PutMapping
    @RequestMapping(value = "/update-product/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public Product updateWholeProduct(@RequestBody Product product, @PathVariable(value = "id") Long id) {
        return productService.updateWholeProduct(product, id);
    }

    //@DeleteMapping
    @RequestMapping(value = "/delete-by-id/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteProductById(@PathVariable(value = "id") Long id) {
        productService.deleteProductById(id);
    }

    //@DeleteMapping
    @RequestMapping(value = "/delete-all", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteAllProducts() {
        productService.deleteAllProducts();
    }
}










